package com.ushakova.tm.exception.system;

import com.ushakova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("***Error: Command " + command + " Not Found.***");
    }

}