package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.empty.*;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User add(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User add(final String login, final String password, final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = add(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User add(final String login, final String password, final Role role) {
        if (role == null) throw new EmptyRoleException();
        final User user = add(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyPasswordException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
