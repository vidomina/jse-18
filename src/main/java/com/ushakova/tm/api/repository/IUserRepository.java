package com.ushakova.tm.api.repository;

import com.ushakova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(final User user);

    List<User> findAll();

    User findByEmail(final String email);

    User findById(final String id);

    User findByLogin(final String login);

    User removeById(final String id);

    User removeByLogin(final String login);

    User removeUser(final User user);

}
