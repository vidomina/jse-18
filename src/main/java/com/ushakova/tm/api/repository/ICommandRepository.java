package com.ushakova.tm.api.repository;

import com.ushakova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    Collection<String> getCommandNames();

    Collection<AbstractCommand> getCommands();

}
