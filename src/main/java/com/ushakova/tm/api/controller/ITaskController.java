package com.ushakova.tm.api.controller;

public interface ITaskController {

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

    void clear();

    void completeTaskById();

    void completeTaskByIndex();

    void completeTaskByName();

    void create();

    void findOneById();

    void findOneByIndex();

    void findOneByName();

    void removeOneById();

    void removeOneByIndex();

    void removeOneByName();

    void showList();

    void startTaskById();

    void startTaskByIndex();

    void startTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

}
