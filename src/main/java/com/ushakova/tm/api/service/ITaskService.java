package com.ushakova.tm.api.service;

import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(String name, String description);

    void add(Task task);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    void clear();

    Task completeTaskById(String id);

    Task completeTaskByIndex(Integer index);

    Task completeTaskByName(String name);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    void remove(Task task);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

}
