package com.ushakova.tm.api.service;

import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.User;

import java.util.List;

public interface IUserService {

    User add(final String login, final String password);

    User add(final String login, final String password, final String email);

    User add(final String login, final String password, final Role role);

    List<User> findAll();

    User findById(final String id);

    User findByLogin(final String login);

    User removeById(final String id);

    User removeByLogin(final String login);

    User removeUser(final User user);

    User setPassword(final String userId, final String password);

    User updateUser(final String userId, final String firstName, final String lastName, final String middleName);

}
