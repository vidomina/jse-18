package com.ushakova.tm.api.service;

import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project add(String name, String description);

    void add(Project project);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

    void clear();

    Project completeProjectById(String id);

    Project completeProjectByIndex(Integer index);

    Project completeProjectByName(String name);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    void remove(Project project);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

}
