package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.util.TerminalUtil;

public class AuthLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login";
    }

    @Override
    public void execute() {
        System.out.println("Enter Login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public String name() {
        return "login-new";
    }

}
