package com.ushakova.tm.command;

import com.ushakova.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    public abstract String name();

    public void setIServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
