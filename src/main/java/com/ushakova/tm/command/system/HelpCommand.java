package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Show terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("\n***Help***");
        final Collection<String> commands = serviceLocator.getCommandService().getCommandNameList();
        for (final String command : commands) {
            if (command == null) continue;
            System.out.println(command);
        }
    }

    @Override
    public String name() {
        return "help";
    }

}
