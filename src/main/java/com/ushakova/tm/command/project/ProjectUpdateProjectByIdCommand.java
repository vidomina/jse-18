package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectUpdateProjectByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by id.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Update Project***\nEnter Id:\"");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateProjectById(id, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

}
